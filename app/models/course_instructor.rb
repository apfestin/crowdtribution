# == Schema Information
#
# Table name: course_instructors
#
#  id            :integer          not null, primary key
#  course_id     :integer
#  instructor_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class CourseInstructor < ActiveRecord::Base
  belongs_to :course
  belongs_to :instructor

  has_many :course_materials
end
