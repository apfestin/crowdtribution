# == Schema Information
#
# Table name: courses
#
#  id         :integer          not null, primary key
#  name       :string
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Course < ActiveRecord::Base
  has_many :course_instructors
  has_many :instructors, through: :course_instructors

  default_scope -> { order(id: :asc) }
end
