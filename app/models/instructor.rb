# == Schema Information
#
# Table name: instructors
#
#  id         :integer          not null, primary key
#  last_name  :string
#  first_name :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Instructor < ActiveRecord::Base
  has_many :course_instructors
  has_many :courses, through: :course_instructors
end
