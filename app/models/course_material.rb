# == Schema Information
#
# Table name: course_materials
#
#  id                   :integer          not null, primary key
#  title                :string
#  description          :string
#  course_instructor_id :integer
#  file_file_name       :string
#  file_content_type    :string
#  file_file_size       :integer
#  file_updated_at      :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class CourseMaterial < ActiveRecord::Base
  has_attached_file :file
  
  do_not_validate_attachment_file_type :file

  validates_attachment_content_type :file, :content_type => /\/(?!(php|pl|exe|pm|cfm|asp)$)/

  belongs_to :course_instructor
end


