class CourseMaterialTypesController < ApplicationController
  before_action :set_course_material_type, only: [:show, :edit, :update, :destroy]

  # GET /course_material_types
  # GET /course_material_types.json
  def index
    @course_material_types = CourseMaterialType.all
  end

  # GET /course_material_types/1
  # GET /course_material_types/1.json
  def show
  end

  # GET /course_material_types/new
  def new
    @course_material_type = CourseMaterialType.new
  end

  # GET /course_material_types/1/edit
  def edit
  end

  # POST /course_material_types
  # POST /course_material_types.json
  def create
    @course_material_type = CourseMaterialType.new(course_material_type_params)

    respond_to do |format|
      if @course_material_type.save
        format.html { redirect_to @course_material_type, notice: 'Course material type was successfully created.' }
        format.json { render :show, status: :created, location: @course_material_type }
      else
        format.html { render :new }
        format.json { render json: @course_material_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /course_material_types/1
  # PATCH/PUT /course_material_types/1.json
  def update
    respond_to do |format|
      if @course_material_type.update(course_material_type_params)
        format.html { redirect_to @course_material_type, notice: 'Course material type was successfully updated.' }
        format.json { render :show, status: :ok, location: @course_material_type }
      else
        format.html { render :edit }
        format.json { render json: @course_material_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /course_material_types/1
  # DELETE /course_material_types/1.json
  def destroy
    @course_material_type.destroy
    respond_to do |format|
      format.html { redirect_to course_material_types_url, notice: 'Course material type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_material_type
      @course_material_type = CourseMaterialType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_material_type_params
      params.require(:course_material_type).permit(:code, :name, :description)
    end
end
