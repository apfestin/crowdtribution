class InstructorsController < ApplicationController
  before_action :set_instructor, only: [:show, :edit, :update, :destroy]

  before_action :set_course
  before_action :set_course_instructor, only: [:show]

  # GET /instructors
  # GET /instructors.json
  def index
    @instructors = @course.instructors

    render json: @instructors
  end

  # GET /instructors/1
  # GET /instructors/1.json
  def show
    render json: @instructor
  end

  # GET /instructors/new
  def new
    @instructor = Instructor.new
  end

  # GET /instructors/1/edit
  def edit
  end

  # POST /instructors
  # POST /instructors.json
  def create
    @instructor = @course.instructors.create(instructor_params)

    respond_to do |format|
      if !@instructor.new_record?
        format.html { redirect_to course_instructors_path(@course), notice: 'Instructor was successfully created.' }
        format.json { render :show, status: :created, location: @instructor }
      else
        format.html { render :new }
        format.json { render json: @instructor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instructors/1
  # PATCH/PUT /instructors/1.json
  def update
    respond_to do |format|
      if @instructor.update(instructor_params)
        format.html { redirect_to [@course, @instructor], notice: 'Instructor was successfully updated.' }
        format.json { render :show, status: :ok, location: @instructor }
      else
        format.html { render :edit }
        format.json { render json: @instructor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /instructors/1
  # DELETE /instructors/1.json
  def destroy
    @instructor.destroy
    respond_to do |format|
      format.html { redirect_to course_instructors_url, notice: 'Instructor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instructor
      @instructor = Instructor.find(params[:id]).decorate
    end

    def set_course
      @course = Course.find(params[:course_id]).decorate
    end

    def set_course_instructor
      @course_instructor = CourseInstructor.where(course: @course, instructor: @instructor).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def instructor_params
      params.require(:instructor).permit(:last_name, :first_name)
    end
end
