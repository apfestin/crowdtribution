class CourseMaterialsController < ApplicationController
  before_action :set_course_material, only: [:show, :edit, :update, :destroy]

  before_action :set_course
  before_action :set_instructor
  before_action :set_course_instructor

  protect_from_forgery with: :null_session

  # GET /course_materials
  # GET /course_materials.json
  def index
    course_materials = @course_instructor.course_materials

    @course_materials = []

    course_materials.each do |course_material|
      @course_materials  << {
        id: course_material.id,
        title: course_material.title,
        description: course_material.description,
        course_instructor_id: course_material.course_instructor_id,
        course_id: course_material.course_instructor.course_id,
        instructor_id: course_material.course_instructor.instructor_id,
        filename: course_material.file_file_name,
        file_link: course_material.file.url,
        file: nil
      }
    end

    render json: @course_materials
  end

  # GET /course_materials/1
  # GET /course_materials/1.json
  def show
  end

  # GET /course_materials/new
  def new
    @course_material = CourseMaterial.new
  end

  # GET /course_materials/1/edit
  def edit
  end

  # POST /course_materials
  # POST /course_materials.json
  def create
    @course_material = @course_instructor.course_materials.new(course_material_params)

    
    if @course_material.save
      render json: @course_material, status: :created
    else
      render json: @course_material.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /course_materials/1
  # PATCH/PUT /course_materials/1.json
  def update
  #   respond_to do |format|
  #     if @course_material.update(course_material_params)
  #       format.html { redirect_to @course_material, notice: 'Course material was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @course_material }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @course_material.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

    if @course_material.update(course_material_params)
      render json: @course_material
    else
      render json: @course_material.errors, status: :unprocessable_entity
    end
  end

  # DELETE /course_materials/1
  # DELETE /course_materials/1.json
  def destroy
    @course_material.destroy

    render json: {}
    # respond_to do |format|
    #   format.html { redirect_to course_materials_url, notice: 'Course material was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_material
      @course_material = CourseMaterial.find(params[:id])
    end

    def set_course
      @course = Course.find(params[:course_id]).decorate
    end

    def set_instructor
      @instructor = Instructor.find(params[:instructor_id]).decorate
    end

    def set_course_instructor
      @course_instructor = CourseInstructor.where(course: @course, instructor: @instructor).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_material_params
      puts "AA"*100
      puts params
      puts "VV"*100
      params.require(:course_material).permit(:title, :description, :file)
    end
end
