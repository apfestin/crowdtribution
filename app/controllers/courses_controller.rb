class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  protect_from_forgery with: :null_session

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.all

    render json: @courses
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
    render json: @course
  end

  # GET /courses/new
  def new
    @course = Course.new
  end

  # GET /courses/1/edit
  def edit
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)

    # --------------
    # MODE: WEB APP
    # ---------------
    # respond_to do |format|
    #   if @course.save
    #     format.html { redirect_to @course, notice: 'Course was successfully created.' }
    #     format.json { render :show, status: :created, location: @course }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @course.errors, status: :unprocessable_entity }
    #   end
    # end

    # ----------
    # MODE: API
    # ----------
    if @course.save
      render json: @course, status: :created, location: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    # --------------
    # MODE: WEB APP
    # ---------------
    # respond_to do |format|
    #   if @course.update(course_params)
    #     format.html { redirect_to @course, notice: 'Course was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @course }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @course.errors, status: :unprocessable_entity }
    #   end
    # end

    # --------------
    # MODE: API
    # ---------------
    if @course.update(course_params)
      render json: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end

  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.delete

    render json: {}

    # --------------
    # MODE: WEB APP
    # ---------------
    # respond_to do |format|
    #   format.html { redirect_to courses_url, notice: 'Course was successfully destroyed.' }
    #   format.json { head :no_content }
    # end


  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id]).decorate
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :title)
    end
end
