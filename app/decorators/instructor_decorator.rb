class InstructorDecorator < Draper::Decorator
  delegate_all

  def last_name
    model.last_name.upcase
  end

  def first_name
    model.first_name.upcase
  end

  def full_name
    [last_name, first_name].compact.join(", ")
  end
end