class CreateCourseInstructors < ActiveRecord::Migration
  def change
    create_table :course_instructors do |t|
      t.references :course, index: true
      t.references :instructor, index: true

      t.timestamps null: false
    end
  end
end
