class CreateCourseMaterials < ActiveRecord::Migration
  def change
    create_table :course_materials do |t|
      t.string :title
      t.string :description
      t.references :course_instructor, index: true

      t.attachment :file

      t.timestamps null: false
    end
  end
end
