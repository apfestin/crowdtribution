# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Course.create(name: 'Philo 1', title: 'Introduction to Philosophy')
Course.create(name: 'Philo 11', title: 'Logic')
Course.create(name: 'Eng 11', title: 'Literature and Society')
Course.create(name: 'PI 100', title: 'Philippine Constitution')
Course.create(name: 'Eng 1', title: 'Basic College English')
Course.create(name: 'Kas 1', title: 'Kasayasayan ng Pilipinas')
Course.create(name: 'Arkiyoloji 1', title: 'Remnants of the Past')

mapping = {
            'Philo 1': [{ first_name: 'Jerwin', last_name: 'Agpaoa' }],
            'Philo 11': [{ first_name: 'Jerwin', last_name: 'Agpaoa' }],
            'Eng 11': [{ first_name: 'Loujaye', last_name: 'Sonido' }],
            'PI 100': [{ first_name: 'Luna', last_name: 'Cleto' }],
            'Eng 1': [{ first_name: 'Lalaine', last_name: 'Aquino' }],
            'Kas 1': [{ first_name: 'Serena', last_name: 'Diokno' }],
            'Arkiyoloji 1': [{ first_name: 'Richard', last_name: 'Tan' }]
          }

Course.all.each do |course|
  instructor_array = mapping[course.name.to_sym]

  instructor_array.each do |instructor_hash|
    course.instructors.create(first_name: instructor_hash[:first_name],
                              last_name: instructor_hash[:last_name])
  end
end

