require 'test_helper'

class CourseMaterialTypesControllerTest < ActionController::TestCase
  setup do
    @course_material_type = course_material_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:course_material_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create course_material_type" do
    assert_difference('CourseMaterialType.count') do
      post :create, course_material_type: { code: @course_material_type.code, description: @course_material_type.description, name: @course_material_type.name }
    end

    assert_redirected_to course_material_type_path(assigns(:course_material_type))
  end

  test "should show course_material_type" do
    get :show, id: @course_material_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @course_material_type
    assert_response :success
  end

  test "should update course_material_type" do
    patch :update, id: @course_material_type, course_material_type: { code: @course_material_type.code, description: @course_material_type.description, name: @course_material_type.name }
    assert_redirected_to course_material_type_path(assigns(:course_material_type))
  end

  test "should destroy course_material_type" do
    assert_difference('CourseMaterialType.count', -1) do
      delete :destroy, id: @course_material_type
    end

    assert_redirected_to course_material_types_path
  end
end
